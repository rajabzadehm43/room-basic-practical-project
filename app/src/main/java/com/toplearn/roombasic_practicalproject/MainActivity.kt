package com.toplearn.roombasic_practicalproject

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.toplearn.roombasic_practicalproject.Adapters.MainAdapter
import com.toplearn.roombasic_practicalproject.RoomLayer.DB.AppDatabase
import com.toplearn.roombasic_practicalproject.RoomLayer.Entities.UserEntity
import com.toplearn.roombasic_practicalproject.Utilities.ImageUtil

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {

    lateinit var db:AppDatabase
    val context = this
    val pickRequest = 436

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        db = AppDatabase.getInstance(context)

        listeners()
        viewInitialzier()
    }


    private fun viewInitialzier() {
        recyclerInit()
    }

    private fun recyclerInit() {

        actv_main_rcy_users.itemAnimator = DefaultItemAnimator()
        actv_main_rcy_users.layoutManager = LinearLayoutManager(context , RecyclerView.VERTICAL , false)

        loadUsers()

    }

    private fun loadUsers() {
        val users = getUsers()
        val userAdapter = MainAdapter(context , users)

        actv_main_rcy_users.adapter = userAdapter
    }

    private fun getUsers() : List<UserEntity> {

        val users = db.users().getAll()

        return users
    }

    private fun listeners() {

        fab.setOnClickListener { view ->
            /*val user1 = UserEntity("mohammad rajabzadeh" , "rajabzadehm43@gmail.com" , "09165796052")
            db.users().insert(user1);*/

            //advancedAddUser()

            openAddOrEdit()
        }

    }

    fun advancedAddUser () {

        val picker = Intent(Intent.ACTION_GET_CONTENT)
        picker.setType("image/*")

        startActivityForResult(picker , pickRequest)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)



        if(requestCode == pickRequest && resultCode == Activity.RESULT_OK) {

            val uImage = data!!.data!!
            val saveImage = ImageUtil.saveFilePrivate(context , uImage)

            addUser("toplearn" , null , "09165976052" , saveImage)

        }

    }

    fun addUser(name:String , email:String? , phone:String , image:String?) {

        db.users().insert(UserEntity(name , email , phone , image))

    }

    private fun openAddOrEdit(id:Int = 0) {
        val open = Intent(context , AddOrEditActivity::class.java)
        if (id != 0)
            open.putExtra("id" , id)
        startActivity(open)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    override fun onRestart() {
        super.onRestart()
        loadUsers()
    }
}
